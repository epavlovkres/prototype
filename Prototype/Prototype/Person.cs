﻿using System;

namespace Prototype
{
	public class Person: IMyCloneable<Person>, ICloneable
	{
		private string _name;
		private string _secondName;
		private bool _isMale;

		public Person(string name, string secondName, bool isMale)
		{
			_name = name;
			_secondName = secondName;
			_isMale = isMale;
		}

		public Person(Person prototype)
		{
			_name = prototype._name;
			_secondName = prototype._secondName;
			_isMale = prototype._isMale;
		}

		public virtual Person Clone()
		{
			return new Person(this);
		}

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;
			Person person = (Person)obj;
			return _name == person._name && _secondName == person._secondName && _isMale == person._isMale;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}
	}
}
