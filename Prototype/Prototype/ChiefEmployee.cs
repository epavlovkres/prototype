﻿using System.Collections.Generic;

namespace Prototype
{
	public class ChiefEmployee : Employee, IMyCloneable<ChiefEmployee>
	{
		private List<Employee> _subordinates;

		public ChiefEmployee(string name, string secondName, bool isMale, string position, List<Employee> subordinates): base(name, secondName, isMale, position)
		{
			_subordinates = subordinates;
		}

		public ChiefEmployee(ChiefEmployee prototype): base(prototype)
		{
			_subordinates = prototype._subordinates;
		}

		public override ChiefEmployee Clone()
		{
			return new ChiefEmployee(this);
		}

		public override bool Equals(object obj)
		{
			var cemployee = (ChiefEmployee)obj;
			return base.Equals(obj) && _subordinates.Equals(cemployee._subordinates);
		}
	}
}
