﻿namespace Prototype
{
	public class Employee: Person, IMyCloneable<Employee>
	{
		private string _position;

		public Employee(string name, string secondName, bool isMale, string position): base(name, secondName, isMale)
		{
			_position = position;
		}

		public Employee(Employee prototype): base(prototype)
		{
			_position = prototype._position;
		}

		public override Employee Clone()
		{
			return new Employee(this);
		}

		public override bool Equals(object obj)
		{
			var employee = (Employee)obj;
			return base.Equals(obj) && _position == employee._position;
		}
	}
}
