using NUnit.Framework;
using Prototype;
using System.Collections.Generic;

namespace Tests
{
	public class Tests
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void Test1()
		{
			var employee1 = new Employee("evgeniy", "pavlov", true, "software engineer");
			var chiefEmployee1 = new ChiefEmployee("someone", "somebodevich", true, "some", new List<Employee> { employee1 });
			var clone = chiefEmployee1.Clone();
			Assert.AreEqual(chiefEmployee1, clone);
		}
	}
}